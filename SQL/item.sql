CREATE TABLE item(
 item_id int PRIMARY KEY AUTO_INCREMENT NOT NULL,
 item_name VARCHAR(256) NOT NULL,
 item_price int,
 category_id int,
 item_material VARCHAR(256),
 item_count int,
 item_image VARCHAR(256),
 item_memo text,
 create_date DATETIME not null default CURRENT_TIMESTAMP,
 update_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
