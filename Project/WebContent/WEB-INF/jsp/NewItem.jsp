<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet" href="CSS/style.css">
<title>新規登録</title>
</head>
<body>
<section class="bg-white text-center py-5">
<div class="text-center" style="padding:50px 0">
<h2>新規作品登録</h2>

<div class="cp_form">
<form method="POST" enctype="multipart/form-data" action="NewItem">

<div class="cp_group">
	<input type="text" name="itemname">
	<label class="cp_label" for="input">作品名</label>
	<i class="bar"></i>
</div>

<div class="cp_group cp_ipselect">
	<select name="categoryid"class="cp_sl" required>
	<c:forEach var="itemcategoryname" items="${itemcategoryname}" >
	<option value="${itemcategoryname.id}">${itemcategoryname.name}</option>
	</c:forEach>
	</select>
	<label class="cp_sl_selectlabel">分類</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="text" name="material">
	<label class="cp_label" for="input">材料</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="text" name="size">
	<label class="cp_label" for="input">大きさ</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="number" name="price">
	<label class="cp_label" for="input">希望販売価格価格</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="number" name="stock">
	<label class="cp_label" for="input">在庫数</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="date" name="production_date">
	<label class="cp_label" for="input">制作年月日</label>
	<i class="bar"></i>
</div>

<div class="cp_group cp_ipselect">
	<select name="box"class="cp_sl" required>
	<option value="有り">有り</option>
	<option value="無し">無し</option>
	</select>
	<label class="cp_sl_selectlabel">桐箱</label>
	<i class="bar"></i>
<div class="cp_group">
	<input type="text" name="memo">
	<label class="cp_label" for="input">備考</label>
	<i class="bar"></i>
</div>

<div class="cp_group">
	<input type="file" name="image"/>
	<label class="cp_label" for="input">画像1(メイン)</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="file" name="image2"/>
	<label class="cp_label" for="input">画像2</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="file" name="image3"/>
	<label class="cp_label" for="input">画像3</label>
	<i class="bar"></i>
</div>
<div class="btn_cont">
	<button class="btn" type="submit" name="action"><span>Submit</span></button>
</div>
</div>
</form>
<a href="ItemList">作品一覧へ戻る</a>