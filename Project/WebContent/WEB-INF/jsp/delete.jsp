<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet" href="CSS/style.css">
<!-- luminousCSS呼び出し -->
<link rel="stylesheet" href="js/luminous-basic.min.css">


<title>作品詳細</title>
</head>
<body>
<form method="POST" action="Delete">
<section class="bg-white text-center py-5">
<div class="text-center" style="padding:50px 0">

<section class="py-5">
        <h1 class="text-center mb-5">削除</h1>
        <div class="container">


          <div class="row">
            <div class="col-sm-7 order-sm-1">
            <p class="sample"><c:if test="${item.image ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br>画像１<br></p></c:if>
        <p class="sample"><p class="sample"><c:if test="${item.image !=  null}">
        <a href="img/${item.image}" class="luminous"><img src="img/${item.image}"></a><br>画像１<br></p></c:if>

            </div>
            <div class="col-sm-2 order-sm-1">

 			<input type="hidden" name="id" value="${item.id}">
              <h3>${item.name}</h3>
					<h5>${item.price}円</h5><br>

            </div>
          </div>

        </div>
      </section>

     本当に削除しますか？<br>
     <input type="submit" value="削除"></div>
      </form>

     <a href="ItemList">作品一覧へ戻る</a>
<!-- luminousJS呼び出し -->
	<script src="js/Luminous.min.js"></script>
	<script src="js/luminous-Trigger.js"></script>
</body>
</html>