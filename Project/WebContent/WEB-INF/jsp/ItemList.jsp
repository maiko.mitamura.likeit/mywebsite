<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c"
	rel="stylesheet">
<link rel="stylesheet" href="CSS/style.css">

<!-- luminousCSS呼び出し -->
<link rel="stylesheet" href="js/luminous-basic.min.css">



<title>作品在庫一覧</title>
</head>
<body>
	<section class="bg-white text-center py-5">
		<div class="text-center" style="padding: 50px 0">
			<h1 class="mb-5">作品管理システム</h1>
			<div class="container">
				<ul class="nav nav-tabs">
					<li class="nav-item"><a href="#ios" class="nav-link active"
						data-toggle="tab">Search</a></li>
					<li class="nav-item"><a href="#new" class="nav-link"
						data-toggle="tab">NewItem</a></li>
					<li class="nav-item"><a href="#sale" class="nav-link"
						data-toggle="tab">Sales record</a></li>
				</ul>
				<div class="tab-content py-4">
					<div id="ios" class="tab-pane active">
						<form action="ItemList" method="post" class="search_container">
							<input type="text" name="itemname" size="25" placeholder="キーワード検索">
							<input type="submit" value="&#xf002">
						</form>
					</div>
					<div id="new" class="tab-pane">
						<h4>
							<a class="new" href="NewItem">新規登録</a>
							</h4>
					</div>
					<div id="sale" class="tab-pane">
						<h4>販売記録</h4>
						<select id="year" name="year" onChange="location.href=value;">
						<option value="">年を選択</option>
						</select>

					</div>
				</div>
				<div class="container">
					<table class="table table-hover tablesorter" id="sort">
						<thead>

							<tr>
								<th>art</th>
								<th>title</th>
								<th><span class="mgr-10">type</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th><span class="mgr-10">Date</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th><span class="mgr-10">stock</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th></th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="itemList" items="${itemList}">
								<tr data-href="Itemdetail?id=${itemList.id}">
									<td>
										<p class="sample">
											<c:if test="${itemList.image ==  null}">
												<a href="img/Noimage.gif" class="luminous"><img
													src="img/Noimage.gif"></a>
												<br>
												<br>
										</p>
										</c:if>
										<p class="sample">
										<p class="sample">
											<c:if test="${itemList.image !=  null}">
												<a href="img/${itemList.image}" class="luminous"><img
													src="img/${itemList.image}"></a>
												<br>
												<br>
										</p>
										</c:if>
									</td>

									<td>${itemList.name}</td>
									<td>${itemList.categoryname}</td>
									<td>${itemList.formatDate}</td>
									<td>${itemList.count}</td>
									<td align="left"><a class="edit-icon"
										href="Update?id=${itemList.id}">edit</a><br>
									<a class="customer-icon" href="Customer?id=${itemList.id}">customer</a><br>
									<a class="delete-icon" href="Delete?id=${itemList.id}">delete</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					</div>
					</div>
	</section>
	<!-- luminousJS呼び出し -->
	<script src="js/Luminous.min.js"></script>
	<script src="js/luminous-Trigger.js"></script>
	<!-- テーブルソーター -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/js/jquery.tablesorter.min.js"></script>
	<script src="js/tablesorter.js"></script>

	<!-- 行全体をリンク -->
	<script>
		jQuery( function($) {
	    $('tbody tr[data-href]').addClass('clickable').click( function() {
 	       window.location = $(this).attr('data-href');
	    }).find('a').hover( function() {
	        $(this).parents('tr').unbind('click');
	    }, function() {
        $(this).parents('tr').click( function() {
            window.location = $(this).attr('data-href');
        });
    });
});
	</script>

<!-- 現在の年を取得&プルダウンでジャンプ -->
<script type="text/javascript">
$(function(){
	var now   = new Date();
	var year  = now.getFullYear();

	for(var i=0;i<10;i++){
		var year2 = year - i;
	    let op = document.createElement("option");
	    op.value = "SalesRecord?year=" + year2 ;  //value値
	    op.text = year2 + "年";   //テキスト値
	    document.getElementById("year").appendChild(op);
	  }
});


</script>


</body>
</html>