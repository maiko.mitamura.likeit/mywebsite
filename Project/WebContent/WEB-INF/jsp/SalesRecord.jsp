<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c"
	rel="stylesheet">
<link rel="stylesheet" href="CSS/style.css">

<!-- luminousCSS呼び出し -->
<link rel="stylesheet" href="js/luminous-basic.min.css">



<title>販売記録</title>
</head>
<body>
	<section class="bg-white text-center py-5">
		<div class="text-center" style="padding: 50px 0">
			<h2>販売記録</h2>
			<h5 style="color:#FF1A6F;">${year}年 1月1日〜12月31日</h5>
			<div class="container">
<div style="float: left;"><a href="SalesRecord?year=${year+1}">←${year+1}年</a></div><div style="float: right;"><a href="SalesRecord?year=${year-1}">${year-1}年→</a></div>
			<div class="mb-5"></div>

					<table class="table table-hover tablesorter" id="sort">
						<thead>

							<tr>

								<th><span class="mgr-10">日付</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
									<th><span class="mgr-10">作品名</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th><span class="mgr-10">金額</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th><span class="mgr-10">個数</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th><span class="mgr-10">合計金額</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th><span class="mgr-10">購入者</span><a class="sortup-icon"
									href="#"></a><a class="sortdown-icon" href="#"></a></th>
								<th>備考</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<form action="SalesRecord" method="post">
								<c:forEach var="customer" items="${customerRecord}">
								<input type="hidden" value="${customer.id}" name="hiddenid">
								<input type="hidden" value="${year}" name="year">
          								<tr>
          									<td><input type="date" value="${customer.saledate}" name="saledate" style="border:none"></td>
          									<td><input type="text" value="${customer.itemname}" readonly="readonly"  style="width:110px; border:none;"></td>
          									<td><input type="number" value="${customer.buyprice}" name="buyprice" style="width:90px; border:none"></td>
          									<td><input type="number" value="${customer.buycount}" name="buycount" style="width:50px; border:none"></td>
          									<td><input type="number" value="${customer.buyprice*customer.buycount}" style="width:90px;border:none"></td>
          									<td><input type="text" value="${customer.customername}" name="customername" style="width:110px; border:none"></td>
          									<td><input type="text" value="${customer.customermemo}" name="customermemo" style="width:110px; border:none"></td>
          									<td><input type="submit" value="更新" style="color:#FF1A6F; border:none"><br></form>
          									<a href="Delete">削除</a></td>
          									</c:forEach>
          									<tr>
          									<td></td>
          									<td></td>
          									<td></td>
          									<td></td>
          									<td></td>
          									<td></td>
          									<td></td>
          								</tr>
						</tbody>
					</table>
					</div>
					</div>
					 <a href="ItemList">作品一覧へ戻る</a>
	</section>
	<!-- luminousJS呼び出し -->
	<script src="js/Luminous.min.js"></script>
	<script src="js/luminous-Trigger.js"></script>
	<!-- テーブルソーター -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/js/jquery.tablesorter.min.js"></script>
	<script src="js/tablesorter.js"></script>

	<!-- 行全体をリンク -->
	<script>
		jQuery( function($) {
	    $('tbody tr[data-href]').addClass('clickable').click( function() {
 	       window.location = $(this).attr('data-href');
	    }).find('a').hover( function() {
	        $(this).parents('tr').unbind('click');
	    }, function() {
        $(this).parents('tr').click( function() {
            window.location = $(this).attr('data-href');
        });
    });
});
	</script>


</body>
</html>