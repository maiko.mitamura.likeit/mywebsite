<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet" href="CSS/style.css">
<!-- luminousCSS呼び出し -->
<link rel="stylesheet" href="js/luminous-basic.min.css">

<title>お客様情報登録</title>
</head>
<body>

<section class="bg-white text-center py-5">
<div class="text-center" style="padding:50px 0">
<h2>お客様情報</h2>
<div class="row">
<div class="col-sm-5"><div class="float-right"><br><br>
<p class="sample"><c:if test="${item.image ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br><br></p></c:if>
        <p class="sample"><c:if test="${item.image !=  null}">
        <a href="img/${item.image}" class="luminous"><img src="img/${item.image}"></a><br></c:if>
          <h5>在庫: ${item.count}点</h5>
          <p>${item.categoryname}</p>
            </div></div>
<div class="row">
 <div class="col-sm-10">
<div class="cp_form">
<form method="POST" action="">
 <input type="hidden" name="id" value="${item.id}">
 <input type="hidden" name="stock" value="${item.count}">
<div class="cp_group">
<div class="container">

                <h4>作品No.${item.id} ${item.name}</h4>
                <div class="cp_group">
    <input type="number" name="item_id" value="${item.id}">
	<label class="cp_label" for="input">作品ID</label>
	<div class="cp_group">
	<input type="number" name="buy_count" value="${item.count}">
	<label class="cp_label" for="input">売上数</label>
	<div class="cp_group">
	<input type="number" name="buy_price" value="${item.price}">
	<label class="cp_label" for="input">売上金額</label>
	<div class="cp_group">
	<input type="date" name="sales_date" value="today">
	<label class="cp_label" for="input">売上日</label>
	<i class="bar"></i>
	</div>
	<div class="cp_group">
	<input type="text" name="customer_name">
	<label class="cp_label" for="input">購入者氏名</label>
	<i class="bar"></i>
</div><div class="cp_group">
	<input type="text" name="customer_memo">
	<label class="cp_label" for="input">メモ</label>
	<i class="bar"></i>
</div>
<div class="btn_cont">
	<button class="btn" type="submit" name="action"><span>Submit</span></button>
</div>

            </div>
          </div>
        </div>
        </form>
        <a href="ItemList">作品一覧へ戻る</a>
      </section>
   <!-- luminousJS呼び出し -->
	<script src="js/Luminous.min.js"></script>
	<script src="js/luminous-Trigger.js"></script>


   <script src="js/today.js"></script>
</body>
</html>