<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet" href="CSS/style.css">
<!-- luminousCSS呼び出し -->
<link rel="stylesheet" href="js/luminous-basic.min.css">

<title>作品詳細</title>
</head>
<body>
<section class="bg-white text-center py-5">
<div class="text-center" style="padding:50px 0">

<section class="py-5">
        <h1 class="text-center mb-5">詳細情報</h1>
        <div class="container">

          <div class="row">
          <div class="col-sm-2">
              <p class="sample"><c:if test="${item.image ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br>画像１<br></p></c:if>
        <p class="sample"><p class="sample"><c:if test="${item.image !=  null}">
        <a href="img/${item.image}" class="luminous"><img src="img/${item.image}"></a><br>画像１<br></p></c:if>

		<p class="sample"><c:if test="${item.image2 ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br>画像２<br></p></c:if>
		<p class="sample"><c:if test="${item.image2 !=  null}">
		<a href="img/${item.image2}" class="luminous"><img src="img/${item.image2}"></a><br>画像２<br></p></c:if>

		<p class="sample"><c:if test="${item.image3 ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br>画像３<br></p></c:if>
		<p class="sample"><c:if test="${item.image3 !=  null}">
		<a href="img/${item.image3}" class="luminous"><img src="img/${item.image3}"></a><br>画像３<br></p></c:if>
            </div>
            <div class="col-sm-5">
              <h3><u>作品No.${item.id} ${item.name}</h3></u><br>

					<h5>${item.price}円</h5><br>
					<p>${item.categoryname}</p><br>
					<p>在庫数 ${item.count}点</p><br>
					<p>材料:${item.material}</p><br>
					<p>サイズ:${item.size}</p><br>
					<p>制作日:${item.productiondate}</p><br>
					<p>桐箱:${item.box}</p><br>
					<p>備考:${item.memo}</p><br>
					<p>データ作成日:${item.createDate}</p>
					<p>データ更新日:${item.updateDate}</p><br>


            </div>
            <div class="col-sm-5">
            <h5>＜販売記録＞</h5><br>
            <c:forEach var="customer" items="${customer}">
					<p>販売日 ${customer.saledate}</p>
					<p>購入者氏名:${customer.customername}</p>
					<p>売上数:${customer.buycount}点</p>
					<p>合計金額:${customer.buycount*customer.buyprice}円</p><br>
					<hr>
					</c:forEach>
            </div>
            </div>
            <a href="ItemList">作品一覧へ戻る</a>
          </div>
        </div>

      </section>

      <!-- luminousJS呼び出し -->
	<script src="js/Luminous.min.js"></script>
	<script src="js/luminous-Trigger.js"></script>
</body>
</html>