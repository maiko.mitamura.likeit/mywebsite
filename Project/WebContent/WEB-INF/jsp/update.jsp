<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet" href="CSS/style.css">
<!-- luminousCSS呼び出し -->
<link rel="stylesheet" href="js/luminous-basic.min.css">

<title>更新</title>
</head>
<body>

<section class="bg-white text-center py-5">
<div class="text-center" style="padding:50px 0">
<h2>update</h2>
<div class="row">
<div class="col-sm-5"><div class="float-right"><br><br>
<p class="sample"><c:if test="${item.image ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br>画像１<br></p></c:if>
        <p class="sample"><c:if test="${item.image !=  null}">
        <a href="img/${item.image}" class="luminous"><img src="img/${item.image}"></a><br>画像１<a href="ImageDelete?id=${item.id}">＜削除＞</a><br></p></c:if>

		<p class="sample"><c:if test="${item.image2 ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br>画像２<br></p></c:if>
		<p class="sample"><c:if test="${item.image2 !=  null}">
		<a href="img/${item.image2}" class="luminous"><img src="img/${item.image2}"></a><br>画像２<a href="Image2Delete?id=${item.id}">＜削除＞</a><br></p></c:if>

		<p class="sample"><c:if test="${item.image3 ==  null}">
		<a href="img/Noimage.gif" class="luminous"><img src="img/Noimage.gif"></a><br>画像３<br></p></c:if>
		<p class="sample"><c:if test="${item.image3 !=  null}">
		<a href="img/${item.image3}" class="luminous"><img src="img/${item.image3}"></a><br>画像３<a href="Image3Delete?id=${item.id}">＜削除＞</a><br></p></c:if>
            </div>
            </div>
<div class="row">
 <div class="col-sm-10">
<div class="cp_form">
<form method="POST" enctype="multipart/form-data" action="Update">
 <input type="hidden" name="id" value="${item.id}">
<div class="cp_group">
	<input type="text" name="itemname" value="${item.name}">
	<label class="cp_label" for="input">作品名</label>
	<i class="bar"></i>
</div>

<div class="cp_group cp_ipselect">
	<select name="categoryid"class="cp_sl" required>
	<c:forEach var="itemcategoryname" items="${itemcategoryname}" >
	<c:if test="${item.categoryid == itemcategoryname.id}">
		<option selected value="${itemcategoryname.id}">${itemcategoryname.name}</option>
	</c:if>
	<c:if test="${item.categoryid != itemcategoryname.id}">
		<option value="${itemcategoryname.id}">${itemcategoryname.name}</option>
	</c:if>
	</c:forEach>
	</select>
	<label class="cp_sl_selectlabel">分類</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="text" name="material" value="${item.material}">
	<label class="cp_label" for="input">材料</label>
	<i class="bar"></i>
<div class="cp_group">
	<input type="text" name="size" value="${item.size}">
	<label class="cp_label" for="input">大きさ</label>
	<i class="bar"></i>
</div>
</div>
<div class="cp_group">
	<input type="number" name="price" value="${item.price}">
	<label class="cp_label" for="input">希望販売価格価格</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="number" name="stock" value="${item.count}">
	<label class="cp_label" for="input">在庫数</label>
	<div class="cp_group">
	<input type="date" name="production_date" value="${item.productiondate}">
	<label class="cp_label" for="input">制作年月日</label>
	<i class="bar"></i>
</div>

<div class="cp_group cp_ipselect">
	<select name="box" class="cp_sl" required>
	<c:if test="${item.box == '有り'}">
		<option selected value="有り">有り</option>
		<option value="無し">無し</option>
	</c:if>
	<c:if test="${item.box == '無し'}">
		<option selected value="無し">無し</option>
		<option value="有り">有り</option>
	</c:if>
	<c:if test="${item.box == null}">
		<option value="無し">無し</option>
		<option value="有り">有り</option>
	</c:if>
	</select>
	<label class="cp_sl_selectlabel">桐箱</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="text" name="memo" value="${item.memo}">
	<label class="cp_label" for="input">備考</label>
	<i class="bar"></i>
</div>

<div class="cp_group">
	<input type="file" name="image" />
	<label class="cp_label" for="input">画像1(メイン)</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="file" name="image2"/>
	<label class="cp_label" for="input">画像2</label>
	<i class="bar"></i>
</div>
<div class="cp_group">
	<input type="file" name="image3"/>
	<label class="cp_label" for="input">画像3</label>
	<i class="bar"></i>
</div>
<div class="btn_cont">
	<button class="btn" type="submit" name="action"><span>Submit</span></button>
</div>
</div>
</form>
<a href="ItemList">作品一覧へ戻る</a>
<!-- luminousJS呼び出し -->
	<script src="js/Luminous.min.js"></script>
	<script src="js/luminous-Trigger.js"></script>
</body>
</html>