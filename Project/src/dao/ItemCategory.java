package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.itemcategorybeans;

public class ItemCategory {

	public ArrayList<itemcategorybeans> findAll() {
        Connection conn = null;
        ArrayList<itemcategorybeans> categoryList = new ArrayList<itemcategorybeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = ("SELECT*FROM item_category");

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int categoryid = rs.getInt("category_id");
                String categoryname = rs.getString("category_name");

                itemcategorybeans categorylist = new itemcategorybeans(categoryid,categoryname);
                categoryList.add(categorylist);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return categoryList;
	}
	//インサート
	public void insertcustomer(String itemname, String categoryid, String material,String size,String price,String stock,String productiondate,String box,String memo,String imageName,String imageName2,String imageName3) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO item(item_name,item_categoryid,item_material,size,item_price,"
					+ "item_count,production_date,box,item_memo,item_image,item_image2,item_image3)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);

			// SQLの?パラメータに値を設定
			stmt.setString(1, itemname);
			stmt.setString(2, categoryid);
			stmt.setString(3, material);
			stmt.setString(4, size);
			stmt.setString(5, price);
			stmt.setString(6, stock);
			stmt.setString(7, productiondate);
			stmt.setString(8, box);
			stmt.setString(9, memo);
			stmt.setString(10, imageName);
			stmt.setString(11, imageName2);
			stmt.setString(12, imageName3);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
