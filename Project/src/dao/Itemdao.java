package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.itembeans;

/*アイテム一覧*/

public class Itemdao {

	 public ArrayList<itembeans> findAll() {
	        Connection conn = null;
	        ArrayList<itembeans> itemList = new ArrayList<itembeans>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();


	            String sql = ("SELECT item_id,item_image,item_name,item_count,production_date,category_name"
	            		+" FROM item"
	            		+" JOIN item_category"
	            		+" ON item.item_categoryid = item_category.category_id");

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	            	int id = rs.getInt("item_id");
	                String image = rs.getString("item_image");
	                String itemname = rs.getString("item_name");
	                int count = rs.getInt("item_count");
	                Date productiondate = rs.getDate("production_date");
	                String categoryname = rs.getString("category_name");

	                itembeans itemdata = new itembeans();
	                itemdata.setId(id);
	                itemdata.setImage(image);
	                itemdata.setName(itemname);
	                itemdata.setCount(count);
	                itemdata.setProductiondate(productiondate);
	                itemdata.setCategoryname(categoryname);
	                itemList.add(itemdata);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return itemList;
	 }
/*詳細*/
	 public itembeans idget(String id) {
			Connection conn = null;

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = ("SELECT item_id,item_name,item_categoryid,item_material,size,item_price,item_count,production_date,box,item_memo,"
						+ "item_image,item_image2,item_image3,category_name,create_date,update_date"
	            		+" FROM item"
	            		+" JOIN item_category"
	            		+" ON item.item_categoryid = item_category.category_id"
	            		+" WHERE item_id = ?");
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, id);
				ResultSet rs = pStmt.executeQuery();
				if (!rs.next()) {
					return null;
				}
                int itemid = rs.getInt("item_id");
                String itemname = rs.getString("item_name");
                int categoryid = rs.getInt("item_categoryid");
                String material = rs.getString("item_material");
                String size = rs.getString("size");
                int price = rs.getInt("item_price");
                int count = rs.getInt("item_count");
                Date productiondate=rs.getDate("production_date");
                String box=rs.getString("box");
                String memo = rs.getString("item_memo");
                String image = rs.getString("item_image");
                String image2 = rs.getString("item_image2");
                String image3 = rs.getString("item_image3");
                String categoryname = rs.getString("category_name");
                String createDate =rs.getString("create_date");
                String updateDate =rs.getString("update_date");

				itembeans item = new itembeans(itemid,itemname, categoryid, material,size, price, count,productiondate, box,memo, image,image2,image3,categoryname,createDate,updateDate);

				return item;

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return null;
		}

/*アップデート*/
public void update(String itemname, String categoryid, String material,String size, String price,String stock,String productiondate,String box,String memo,String imageName,String imageName2,String imageName3,String hiddenID) {
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_name=?,item_categoryid=?, item_material=? ,size=?,item_price=?,"
				+ "item_count=?,production_date=?,box=?,item_memo=?,item_image=?,item_image2=?,item_image3=?"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, itemname);
		stmt.setString(2, categoryid);
		stmt.setString(3, material);
		stmt.setString(4, size);
		stmt.setString(5, price);
		stmt.setString(6, stock);
		stmt.setString(7, productiondate);
		stmt.setString(8, box);
		stmt.setString(9, memo);
		stmt.setString(10, imageName);
		stmt.setString(11, imageName2);
		stmt.setString(12, imageName3);
		stmt.setString(13, hiddenID);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
//画像無しアップデート
public void update2(String itemname, String categoryid, String material,String size, String price,String stock,String productiondate,String box,String memo,String hiddenID) {
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_name=?,item_categoryid=?, item_material=? ,size=?,item_price=?,"
				+ "item_count=?,production_date=?,box=?,item_memo=?"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, itemname);
		stmt.setString(2, categoryid);
		stmt.setString(3, material);
		stmt.setString(4, size);
		stmt.setString(5, price);
		stmt.setString(6, stock);
		stmt.setString(7, productiondate);
		stmt.setString(8, box);
		stmt.setString(9, memo);
		stmt.setString(10, hiddenID);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
//画像２だけアップデート
public void update3(String itemname, String categoryid, String material,String size, String price,String stock,String productiondate,String box,String memo,String imageName2,String hiddenID){
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_name=?,item_categoryid=?, item_material=? ,size=?,item_price=?,"
				+ "item_count=?,production_date=?,box=?,item_memo=?,item_image2=?"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, itemname);
		stmt.setString(2, categoryid);
		stmt.setString(3, material);
		stmt.setString(4, size);
		stmt.setString(5, price);
		stmt.setString(6, stock);
		stmt.setString(7, productiondate);
		stmt.setString(8, box);
		stmt.setString(9, memo);
		stmt.setString(10,imageName2);
		stmt.setString(11, hiddenID);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
//画像3だけアップデート
public void update4(String itemname, String categoryid, String material,String size, String price,String stock,String productiondate,String box,String memo,String imageName3,String hiddenID){
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_name=?,item_categoryid=?, item_material=? ,size=?,item_price=?,"
				+ "item_count=?,production_date=?,box=?,item_memo=?,item_image3=?"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, itemname);
		stmt.setString(2, categoryid);
		stmt.setString(3, material);
		stmt.setString(4, size);
		stmt.setString(5, price);
		stmt.setString(6, stock);
		stmt.setString(7, productiondate);
		stmt.setString(8, box);
		stmt.setString(9, memo);
		stmt.setString(10,imageName3);
		stmt.setString(11, hiddenID);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
//画像1だけアップデート
public void update5(String itemname, String categoryid, String material,String size, String price,String stock,String productiondate,String box,String memo,String imageName,String hiddenID){
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_name=?,item_categoryid=?, item_material=? ,size=?,item_price=?,"
				+ "item_count=?,production_date=?,box=?,item_memo=?,item_image=?"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, itemname);
		stmt.setString(2, categoryid);
		stmt.setString(3, material);
		stmt.setString(4, size);
		stmt.setString(5, price);
		stmt.setString(6, stock);
		stmt.setString(7, productiondate);
		stmt.setString(8, box);
		stmt.setString(9, memo);
		stmt.setString(10,imageName);
		stmt.setString(11, hiddenID);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
// インサート
public void insertItem(String itemname, String categoryid, String material,String size,String price,String stock,String productiondate,String box,String memo,String imageName,String imageName2,String imageName3) {
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "INSERT INTO item(item_name,item_categoryid,item_material,size,item_price,"
				+ "item_count,production_date,box,item_memo,item_image,item_image2,item_image3)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);

		// SQLの?パラメータに値を設定
		stmt.setString(1, itemname);
		stmt.setString(2, categoryid);
		stmt.setString(3, material);
		stmt.setString(4, size);
		stmt.setString(5, price);
		stmt.setString(6, stock);
		stmt.setString(7, productiondate);
		stmt.setString(8, box);
		stmt.setString(9, memo);
		stmt.setString(10, imageName);
		stmt.setString(11, imageName2);
		stmt.setString(12, imageName3);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

/*作品検索*/
public ArrayList<itembeans> getItemName(String searchWord) {
	Connection con = null;
	PreparedStatement st = null;

	try {
		con = DBManager.getConnection();
		st = con.prepareStatement("SELECT item_id,item_image,item_name,item_count,production_date,item_memo,category_name"
					+" FROM item"
					+" JOIN item_category"
					+" ON item.item_categoryid = item_category.category_id"
					+ " WHERE item_name like ? or item_memo like ? or category_name like ? or production_date like ?");

			st.setString(1,"%"+searchWord+"%");
			st.setString(2,"%"+searchWord+"%");
			st.setString(3,"%"+searchWord+"%");
			st.setString(4,"%"+searchWord+"%");

		ResultSet rs = st.executeQuery();
		ArrayList<itembeans> itemList = new ArrayList<itembeans>();

			 while (rs.next()) {
	            	int id = rs.getInt("item_id");
	                String image = rs.getString("item_image");
	                String itemname = rs.getString("item_name");
	                int count = rs.getInt("item_count");
	                Date productiondate =rs.getDate("production_date");
	                String itemmemo = rs.getString("item_memo");
	                String categoryname = rs.getString("category_name");

	                itembeans itemdata = new itembeans(id,image,itemname,count,productiondate,itemmemo,categoryname);
	                itemList.add(itemdata);
	            }
		System.out.println("get Items by itemName has been completed");
		return itemList;
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (st != null) {
				st.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return null;
	}
//画像１選択
public itembeans imageget(String id) {
	Connection conn = null;

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		String sql = ("select item_image from item where item_id=?");
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();
		if (!rs.next()) {
			return null;
		}
        String image = rs.getString("item_image");

		itembeans getimage = new itembeans();
		getimage.setImage(image);

		return getimage;

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	return null;
}
//画像１削除
public void imageupdate(String id){
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_image=null"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, id);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
//画像２選択
public itembeans image2get(String id) {
	Connection conn = null;

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		String sql = ("select item_image2 from item where item_id=?");
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();
		if (!rs.next()) {
			return null;
		}
        String image2 = rs.getString("item_image2");

        itembeans getimage2 = new itembeans();
		getimage2.setImage2(image2);

		return getimage2;

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	return null;
}
//画像２削除
public void image2update(String id){
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_image2=null"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, id);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
//画像３選択
public itembeans image3get(String id) {
	Connection conn = null;

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		String sql = ("select item_image3 from item where item_id=?");
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();
		if (!rs.next()) {
			return null;
		}
        String image3 = rs.getString("item_image3");

		itembeans getimage3 = new itembeans();
		getimage3.setImage3(image3);

		return getimage3;

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	return null;
}
//画像３削除
public void image3update(String id){
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_image3=null"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setString(1, id);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

//在庫アップデート（在庫-売り上げ）
public void countupdate(int result,String itemid){
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();
		// 実行SQL文字列定義
		String insertSQL = "UPDATE item SET item_count=?"
				+ " WHERE item_id=?";
		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);


		// SQLの?パラメータに値を設定
		stmt.setInt(1, result);
		stmt.setString(2, itemid);

		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}






//deleteDao
	public void delete(String hiddenID) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String deleteSQL = "DELETE FROM item WHERE item_id = ?";
			// ステートメント生成
			stmt = con.prepareStatement(deleteSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, hiddenID);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	}
