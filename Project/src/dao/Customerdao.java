package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Customerbeans;

public class Customerdao {
	public void insertCustomer(String itemid, String customername,String saledate,String buycount,String price,String memo) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO customer(customer_item_id,customer_name,customer_sales_date,customer_buycount,"
					+ "customer_itemprice,customer_memo)"
					+ " VALUES (?,?,?,?,?,?)";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);

			// SQLの?パラメータに値を設定
			stmt.setString(1, itemid);
			stmt.setString(2, customername);
			stmt.setString(3, saledate);
			stmt.setString(4, buycount);
			stmt.setString(5, price);
			stmt.setString(6, memo);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//詳細情報
	 public ArrayList<Customerbeans> getCustomer(String id) {
			Connection conn = null;
			PreparedStatement st = null;

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				st = conn.prepareStatement("SELECT*FROM customer WHERE customer_item_id = ?");

				st.setString(1,id);

				ResultSet rs = st.executeQuery();
				ArrayList<Customerbeans> customerList = new ArrayList<Customerbeans>();

		while (rs.next()) {
             int ID=rs.getInt("id");
             int itemid=rs.getInt("customer_item_id");
             String name=rs.getString("customer_name");
             Date salesdate=rs.getDate("customer_sales_date");
             int buycount=rs.getInt("customer_buycount");
             int price=rs.getInt("customer_itemprice");
             String memo=rs.getString("customer_memo");
             String createDate=rs.getString("create_date");
             String updateDate=rs.getString("update_date");

				Customerbeans customer = new Customerbeans(ID,itemid,name,salesdate,buycount,price,memo,createDate,updateDate);

				customerList.add(customer);
		}
				return customerList;
			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (st != null) {
						st.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return null;
			}

	 //販売記録
	 public ArrayList<Customerbeans> getCustomerRecord(String year) {
			Connection conn = null;
			PreparedStatement st = null;

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				st = conn.prepareStatement("SELECT*FROM customer INNER JOIN item ON customer.customer_item_id=item_id WHERE customer_sales_date LIKE ? ORDER BY customer_sales_date");

				st.setString(1,"%"+year+"%");

				ResultSet rs = st.executeQuery();
				ArrayList<Customerbeans> customerList = new ArrayList<Customerbeans>();

		while (rs.next()) {
		Customerbeans customer = new Customerbeans();
          customer.setId(rs.getInt("id"));
          customer.setItemid(rs.getInt("customer_item_id"));
          customer.setCustomername(rs.getString("customer_name"));
          customer.setSaledate(rs.getDate("customer_sales_date"));
          customer.setBuycount(rs.getInt("customer_buycount"));
          customer.setBuyprice(rs.getInt("customer_itemprice"));
          customer.setCustomermemo(rs.getString("customer_memo"));
          customer.setItemname(rs.getString("item_name"));
          customerList.add(customer);
		}
				return customerList;
			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (st != null) {
						st.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return null;
			}

	 //販売記録アップデート
	 public void salesUPdate(String saledate,String buyprice,String buycount, String customername,String customermemo,String hiddenID){
			Connection con = null;
			PreparedStatement stmt = null;

			try {
				// データベース接続
				con = DBManager.getConnection();
				// 実行SQL文字列定義
				String insertSQL = "UPDATE customer SET customer_sales_date=?,customer_itemprice=? ,customer_buycount=?,customer_name=?,"
						+ "customer_memo=?"
						+ " WHERE id=?";
				// ステートメント生成
				stmt = con.prepareStatement(insertSQL);


				// SQLの?パラメータに値を設定
				stmt.setString(1, saledate);
				stmt.setString(2, buyprice);
				stmt.setString(3, buycount);
				stmt.setString(4, customername);
				stmt.setString(5, customermemo);
				stmt.setString(6, hiddenID);
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (stmt != null) {
						stmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (con != null) {
						con.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
}
