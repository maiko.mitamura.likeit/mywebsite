package beans;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class itembeans {
	private int id;
	private String name;
	private int price;
	private int categoryid;
	private String material;
	private String size;
	private int count;
	private Date productiondate;
	private String box;
	private String image;
	private String image2;
	private String image3;
	private String memo;
	private String createDate;
	private String updateDate;

	private String categoryname;

	//引数無し
	public itembeans() {
	}

	public itembeans(int id, String image, String itemname, int count, Date productiondate, String itemmemo,String categoryname) {
		this.id=id;
		this.image=image;
		this.name=itemname;
		this.count=count;
		this.productiondate=productiondate;
		this.memo=itemmemo;
		this.categoryname=categoryname;

		// TODO 自動生成されたコンストラクター・スタブ
	}
	public itembeans(int itemid,String itemname, int categoryid, String material,String size,int price, int count, Date productiondate,String box,String memo,
			String image, String image2, String image3, String categoryname,String createDate,String updateDate) {
		this.id=itemid;
		this.name=itemname;
		this.categoryid=categoryid;
		this.material=material;
		this.size=size;
		this.price=price;
		this.count=count;
		this.productiondate=productiondate;
		this.box=box;
		this.memo=memo;
		this.image=image;
		this.image2=image2;
		this.image3=image3;
		this.categoryname=categoryname;
		this.createDate=createDate;
		this.updateDate=updateDate;

		// TODO 自動生成されたコンストラクター・スタブ
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
	public String getImage3() {
		return image3;
	}
	public void setImage3(String image3) {
		this.image3 = image3;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getCategoryname() {
		return categoryname;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

	public String getBox() {
		return box;
	}
	public void setBox(String box) {
		this.box = box;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public Date getProductiondate() {
		return productiondate;
	}
	public void setProductiondate(Date productiondate) {
		this.productiondate = productiondate;
	}
	public String getFormatDate() {
		if(this.productiondate == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年");
		return sdf.format(productiondate);
	}


}
