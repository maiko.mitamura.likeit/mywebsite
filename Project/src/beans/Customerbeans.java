package beans;

import java.sql.Date;

public class Customerbeans {
	private int id;
	private int itemid;
	private String customername;
	private Date saledate;
	private int buycount;
	private int buyprice;
	private String customermemo;
	private String createDate;
	private String updateDate;

	private String itemname;

	public Customerbeans() {

	}

	public Customerbeans(int ID, int itemid, String name, Date salesdate, int buycount, int price, String memo,
			String createDate, String updateDate) {
		this.id=ID;
		this.itemid=itemid;
		this.customername=name;
		this.saledate=salesdate;
		this.buycount=buycount;
		this.buyprice=price;
		this.customermemo=memo;
		this.createDate=createDate;
		this.updateDate=updateDate;
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getItemid() {
		return itemid;
	}
	public void setItemid(int itemid) {
		this.itemid = itemid;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public Date getSaledate() {
		return saledate;
	}
	public void setSaledate(Date saledate) {
		this.saledate = saledate;
	}
	public int getBuycount() {
		return buycount;
	}
	public void setBuycount(int buycount) {
		this.buycount = buycount;
	}
	public int getBuyprice() {
		return buyprice;
	}
	public void setBuyprice(int buyprice) {
		this.buyprice = buyprice;
	}
	public String getCustomermemo() {
		return customermemo;
	}
	public void setCustomermemo(String customermemo) {
		this.customermemo = customermemo;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	/*public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年1月1日〜12月31日");
		return sdf.format(saledate);
	}*/

}
