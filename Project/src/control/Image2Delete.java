package control;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.itembeans;
import dao.Itemdao;

/**
 * Servlet implementation class Image3Delete
 */
@WebServlet("/Image2Delete")
public class Image2Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Image2Delete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		System.out.println(id);

		Itemdao itemdao = new Itemdao();
		itembeans image2 = itemdao.image2get(id);
		System.out.println(image2.getImage2());

		File file = new File("/Users/maiko/Documents/Git/MyWebSite/Project/WebContent/img/" + image2.getImage2());

		if (file.delete()) {
			System.out.println("ファイルを削除しました");

		itemdao.image2update(id);

			response.sendRedirect("ItemList");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
