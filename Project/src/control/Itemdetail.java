package control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Customerbeans;
import beans.itembeans;
import beans.itemcategorybeans;
import dao.Customerdao;
import dao.ItemCategory;
import dao.Itemdao;

/**
 * Servlet implementation class Itemdetail
 */
@WebServlet("/Itemdetail")
public class Itemdetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Itemdetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);
		ItemCategory itemcategory=new ItemCategory();
		ArrayList<itemcategorybeans> itemcategoryname=itemcategory.findAll();

		request.setAttribute("itemcategoryname",itemcategoryname);

		Itemdao itemdao=new Itemdao();
		itembeans item = itemdao.idget(id);
		request.setAttribute("item",item);

		Customerdao customerdao = new Customerdao();
		ArrayList<Customerbeans> customer=customerdao.getCustomer(id);
		request.setAttribute("customer",customer);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Itemdetail.jsp");
		dispatcher.forward(request, response);
		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
