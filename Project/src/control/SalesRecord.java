package control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Customerbeans;
import dao.Customerdao;

/**
 * Servlet implementation class SalesRecord
 */
@WebServlet("/SalesRecord")
public class SalesRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SalesRecord() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String year = request.getParameter("year");
		System.out.println(year);
		Customerdao customerdao = new Customerdao();
		ArrayList<Customerbeans> customerRecord =customerdao.getCustomerRecord(year);

		request.setAttribute("year",year);
		request.setAttribute("customerRecord",customerRecord);

		RequestDispatcher dispatcher2 = request.getRequestDispatcher("/WEB-INF/jsp/SalesRecord.jsp");
		dispatcher2.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String year = request.getParameter("year");

		String saledate = request.getParameter("saledate");
		String buyprice = request.getParameter("buyprice");
		String buycount = request.getParameter("buycount");
		String customername = request.getParameter("customername");
		String customermemo = request.getParameter("customermemo");
		String hiddenID = request.getParameter("hiddenid");

		Customerdao customerdao = new Customerdao();
		customerdao.salesUPdate(saledate, buyprice, buycount, customername, customermemo, hiddenID);

		response.sendRedirect("SalesRecord?year=" + year);

	}
}
