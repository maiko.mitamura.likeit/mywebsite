package control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.itembeans;
import beans.itemcategorybeans;
import dao.Customerdao;
import dao.ItemCategory;
import dao.Itemdao;

/**
 * Servlet implementation class Customer
 */
@WebServlet("/Customer")
public class Customer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Customer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);
		ItemCategory itemcategory=new ItemCategory();
		ArrayList<itemcategorybeans> itemcategoryname=itemcategory.findAll();

		request.setAttribute("itemcategoryname",itemcategoryname);

		Itemdao itemdao=new Itemdao();
		itembeans item = itemdao.idget(id);
		request.setAttribute("item",item);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/customer.jsp");
		dispatcher.forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

        String itemid = request.getParameter("item_id");
        String customername = request.getParameter("customer_name");
        String saledate= request.getParameter("sales_date");
        String buycount=request.getParameter("buy_count");
        String price=request.getParameter("buy_price");
        String memo=request.getParameter("customer_memo");
        
        Customerdao customerdao = new Customerdao();
        customerdao.insertCustomer(itemid, customername, saledate, buycount, price, memo);
        
        //在庫数アップデート
        String stock=request.getParameter("stock");
        int s = Integer.parseInt(stock);
        int b = Integer.parseInt(buycount);
        int result = s-b;
        
        Itemdao itemdao = new Itemdao();
        itemdao.countupdate(result, itemid);
        
        
        response.sendRedirect("ItemList");
		
	}

}
