package control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.itemcategorybeans;
import dao.ItemCategory;
import dao.Itemdao;

/**
 * Servlet implementation class NewItem
 */
@WebServlet("/NewItem")
@MultipartConfig(location="/Users/maiko/Documents/Git/MyWebSite/Project/WebContent/img", maxFileSize=104857600)
public class NewItem extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ItemCategory itemcategory=new ItemCategory();
		ArrayList<itemcategorybeans> itemcategoryname=itemcategory.findAll();

		request.setAttribute("itemcategoryname",itemcategoryname);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewItem.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

        String itemname = request.getParameter("itemname");
        String categoryid = request.getParameter("categoryid");
        String material = request.getParameter("material");
        String size= request.getParameter("size");
        String price=request.getParameter("price");

        if(price.equals("")) {
        	price=null;
        }
        String stock=request.getParameter("stock");
        if(stock.equals("")) {
        	stock=null;
        }
        String productiondate=request.getParameter("production_date");
        if(productiondate.equals("")) {
        	productiondate=null;
        }
        String box=request.getParameter("box");
        String memo=request.getParameter("memo");

        Part part = request.getPart("image");
        String imageName = null;
        if(part.getSize()>0) {
	        imageName = getFileName(part);
	        part.write(imageName);
        }


        Part part2 = request.getPart("image2");
        String imageName2 = null;
        if(part2.getSize()>0) {
        	imageName2 = getFileName(part2);
        	part2.write(imageName2);
        }


        Part part3 = request.getPart("image3");
        String imageName3 = null;
        if(part3.getSize()>0) {
        imageName3 = getFileName(part3);
        part3.write(imageName3);
        }

        Itemdao itemDao = new Itemdao();

        if(itemname.equals("")) {
        	request.setAttribute("errMsg", "作品名を入力してください");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewItem.jsp");
			dispatcher.forward(request, response);
			return;
        }

        itemDao.insertItem(itemname, categoryid, material, size,price, stock,productiondate,box, memo, imageName, imageName2, imageName3);

        response.sendRedirect("ItemList");
	}

	  private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }

}
